import util from 'node:util'
import { execFile } from 'node:child_process'
import { strict as nodeAssert } from 'assert'

import { expect } from 'chai'
import ram from 'random-access-memory'
import setupServer from 'hypercore-rehost-server'
import Hyperswarm from 'hyperswarm'
import createTestnet from 'hyperdht/testnet.js'
import Signal from 'signal-promise'
import Corestore from 'corestore'
import Rehoster from 'hypercore-rehoster'
import SwarmManager from 'swarm-manager'
import safetyCatch from 'safety-catch'

describe('Rehost-cli tests', function () {
  let testnet
  let swarm, swarmManager
  let server
  const key1 = 'a'.repeat(64)
  const key2 = 'b'.repeat(64)
  let port

  describe('Test happy path', function () {
    this.beforeEach(async function () {
      testnet = await createTestnet(3)
      const bootstrap = testnet.bootstrap
      swarm = new Hyperswarm({ bootstrap })
      const corestore = new Corestore(ram)
      swarmManager = new SwarmManager(swarm)
      swarm.on('connection', socket => {
        corestore.replicate(socket)
        corestore.on('error', safetyCatch)
      })

      const rehoster = new Rehoster(corestore, swarmManager)

      server = await setupServer(rehoster)
      port = `${server.server.address().port}`
    })

    this.afterEach(async function () {
      await swarm.destroy()
      await testnet.destroy()
      const s = new Signal()
      server.close(() => s.notify())
      await s.wait()
    })

    it('Can interact with the CLI (integration test)', async function () {
      const addCommand = ['./index.js', 'add', '--port', port, key1]
      const listCommand = ['./index.js', 'list', '--port', port]
      const infoCommand = ['./index.js', 'info', '--port', port]
      const delCommand = ['./index.js', 'delete', '--port', port, key1]

      const info = 'I describe the content'

      let res = await util.promisify(execFile)('node', addCommand)
      expect(res.stdout).to.equal(`Added key ${key1}\n`)

      res = await util.promisify(execFile)('node', ['./index.js', 'add', key2, '--port', port, '--info', info])
      expect(res.stdout).to.equal(`Added key ${key2} with info '${info}'\n`)

      res = await util.promisify(execFile)('node', listCommand)
      expect(res.stdout).to.equal(`All hosted keys:\n  - ${key1}\n  - ${key2} (I describe the content)\n`)
      expect(res.error).to.equal(undefined)

      res = await util.promisify(execFile)('node', infoCommand)
      expect(res.stdout.split('\n')[1]).to.equal(
        'Rehoster'
      )

      res = await util.promisify(execFile)('node', delCommand)
      expect(res.stdout).to.equal(`Deleted key ${key1}\n`)

      res = await util.promisify(execFile)('node', listCommand)
      expect(res.stdout).to.equal(`All hosted keys:\n  - ${key2} (I describe the content)\n`)
      expect(res.error).to.equal(undefined)
    })
  })

  describe('Throws sensible error on server not found', async function () {
    const addCommand = ['./index.js', 'add', '-p', 59383, key1]
    const listCommand = ['./index.js', 'list', '-p', 59383]
    const infoCommand = ['./index.js', 'info', '-p', 59383]
    const delCommand = ['./index.js', 'delete', '-p', 59383, key1]

    const allCommands = [addCommand, listCommand, infoCommand, delCommand]

    allCommands.forEach(async (command) => {
      it(`Errors sensible when no server for command ${command}`, async function () {
        await nodeAssert.rejects(
          util.promisify(execFile)('node', command),
          { message: /Could not connect to server/ }
        )
      })
    })
  })
})
