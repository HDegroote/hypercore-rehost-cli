#! /usr/bin/env node

import axios from 'axios'
import { program } from 'commander'

const DEFAULT_PORT = 18580
const DEFAULT_HOST = '127.0.0.1'

async function main () {
  program.name('rehost').description('CLI for a hypercore rehost server')

  program
    .command('add')
    .option('-p, --port <int>', 'Port on which the rehost server runs', DEFAULT_PORT)
    .option('-h, --host <string>', 'Host on which the rehost server runs', DEFAULT_HOST)
    .option('-i, --info <string>', 'Brief description of what this key contains (optional)')
    .argument('<key>', 'Hypercore public key to add')
    .action((key, opts) => runAdd(key, opts))
    .description('Add a new public key')

  program
    .command('delete')
    .option('-p, --port <int>', 'Port on which the rehost server runs', DEFAULT_PORT)
    .option('-h, --host <string>', 'Host on which the rehost server runs', DEFAULT_HOST)
    .argument('<key>', 'Hypercore public key to delete')
    .action((key, opts) => runDel(key, opts))
    .description('Delete a public key (nothing happens if the key was not added)')

  program
    .command('list')
    .option('-p, --port <int>', 'Port on which the rehost server runs', DEFAULT_PORT)
    .option('-h, --host <string>', 'Host on which the rehost server runs', DEFAULT_HOST)
    .action((opts) => runList(opts))
    .description('List all public keys in the db')

  program
    .command('info')
    .option('-p, --port <int>', 'Port on which the rehost server runs', DEFAULT_PORT)
    .option('-h, --host <string>', 'Host on which the rehost server runs', DEFAULT_HOST)
    .action((opts) => runInfo(opts))
    .description('Prints info about the keys hosted by the rehoster')

  await program.parseAsync(process.argv)
}

function getUrl (endpoint, { host, port }) {
  return `http://${host}:${port}/${endpoint}`
}

function _handleError (error, description) {
  if (error.message.includes('connect ECONNREFUSED')) {
    console.error('Could not connect to server: ', error.message)
  } else {
    try {
      console.error(`${description}: status ${error.response.status}--${error.response.data.message}`)
    } catch {
      console.error('Unexpected error: ', error)
    }
  }
  process.exit(1)
}

async function runAdd (key, opts) {
  const { info } = opts
  try {
    await axios.put(getUrl(key, opts), { info, key })
    const msg = `Added key ${key}` + (opts.info
      ? ` with info '${opts.info}'`
      : '')
    console.log(msg)
  } catch (error) {
    _handleError(error, 'Error while adding key')
  }
}

async function runDel (key, opts) {
  try {
    await axios.delete(getUrl(key, opts))
    console.log('Deleted key', key)
  } catch (error) {
    _handleError(error, 'Error while deleting key')
  }
}

async function runList (opts) {
  let entries
  try {
    entries = (await axios.get(getUrl('', opts))).data
  } catch (error) {
    _handleError(error, 'Error while listing keys')
  }
  const lines = ['All hosted keys:']
  for (const entry of entries) {
    let res = `  - ${entry.key}`
    if (entry.info) res += ` (${entry.info})`

    lines.push(res)
  }
  console.log(lines.join('\n'))
}

async function runInfo (opts) {
  try {
    const res = await axios.get(getUrl('info', opts))
    console.log(res.data.info, '\n')
    console.log(res.data.details)
  } catch (error) {
    _handleError(error, 'Error while getting info')
  }
}

main()
