# Hypercore Rehost CLI

DEPRECATED, use https://gitlab.com/dcent-tech/rehoster instead.

A command-line interface to facilitate rehosting hypercores.
## Install

`npm install -g hypercore-rehost-cli`

## Prerequisites

Make sure a rehost server is running, and note the port.

Assumes hypercore-rehost-server ^1.0.0

```rehost-server --PORT 40000```

(if hypercore-rehost-server is not yet installed, run `npm i -g hypercore-rehost-server`)

## Usage
Assume the rehoster is running on port 40000.

Note: the -p argument is not required if the rehoster is running on port 50000.

### `rehoster add -p 40000 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`
Add a public key

Accepts a -i option to briefly specify what the key refers to.

### `rehoster list -p 40000`
List all public keys in the db

### `rehoster delete -p 40000 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`
Remove a public key

### `rehoster info -p 40000`
Print info about the keys hosted by the rehoster
